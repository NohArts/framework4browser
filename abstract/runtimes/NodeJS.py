from abstract.commons import *

##################################################################################################################

@Builder.compass('chrome')
class Chrome(Builder):
    def booting(self):
        pass

    #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    def manifest(self):
        payload = {
	        "name": specs['title'],
	        "version": specs['version'],
	        "description": specs['summary'],
	        "icons": {
                "16": "kimino/logo.png",
                "48": "kimino/logo.png",
                "128": "kimino/logo.png"
	        },
	        "browser_action": {
                "default_title": specs['title'],
		        "default_icon": "kimino/logo.png"
	        },
            "content_scripts": [{
                "matches": [
                    "<all_urls>"
                ],
                "js": [
                    "kimino/js/jquery.min.js",
                    "kimino/js/jquery-csv.min.js",
                    "kimino/js/jquery-bootbox.min.js",

                    "kimino/inc/action.js",
                    "kimino/inc/reload.js",
                ] + ["program/%s" % key for key in specs['chrome']['content']],
                "run_at": "document_end"
            }],
            "background": {
                "scripts": [
                    "kimino/js/lodash.js",

                    "kimino/inc/config.js",
                    "kimino/inc/action.js",
                ] + ["program/%s" % key for key in specs['chrome']['content']],
            },
            "content_security_policy": "script-src 'unsafe-eval' 'self' 'unsafe-inline' 'all'; object-src 'self' 'all'",
            "permissions": specs['chrome']['permissions'],
            "manifest_version": 2,
            "update_url": "https://clients2.google.com/service/update2/crx"
        }

        self.write_json(self.rpath('manifest.json', payload))

##################################################################################################################

@Builder.compass('electron')
class Electron(Builder):
    def booting(self):
        pass

    #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    def manifest(self):
        pass

