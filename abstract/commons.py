from .utils import *

##################################################################################################################

Registry = {}

class Factory(object):
    @classmethod
    def compass(cls, *args, **kwargs):
        def do_apply(handler, alias, **options):
            if cls not in Registry:
                Registry[cls] = {}

            if alias not in Registry[cls]:
                Registry[cls][alias] = handler

            return handler

        return lambda hnd: do_apply(hnd, *args, **kwargs)

    #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    @classmethod
    def resolve(cls, alias, *args, **kwargs):
        resp = {}

        if cls in Registry:
            if alias in Registry[cls]:
                handler = Registry[cls][alias]

                if callable(handler):
                    resp = handler(*args, **kwargs)

        return resp

    #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    def __init__(self, *args, **kwargs):
        self.trigger('initialize', *args, **kwargs)

    #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    def trigger(self, method, *args, **kwargs):
        hnd = getattr(self, method, None)

        if callable(hnd):
            return hnd(*args, **kwargs)
        else:
            raise NotImplemented()

#*****************************************************************************************************************

class PosixMixin:
    def lsdir(self, target, **options):
        for alias,default in dict(files=True, dirs=True).iteritems():
            options[alias] = options.get(alias, None) or default

        if os.path.isdir(target):
            for item in os.listdir(target):
                entry = os.path.join(target, item)

                state = (
                    os.path.isdir(entry) and options['dirs']
                or
                    os.path.isfile(entry) and options['files']
                )

                if state:
                    yield entry
    
    #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    def copy(self, target, *origins):
        self.shell('cp','-t',target,'-aR',*origins)
    
    #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    def shell(self, prog, *params):
        stmt = [prog]

        for part in params:
            if ' ' in part:
                part = '"' + part + '"'

            stmt.append(part)

        stmt = ' '.join(stmt)

        os.system(stmt)
    
    #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    def read_text(self, target): return open(target).read()
    def read_json(self, target): return json.load(open(target))
    def read_yaml(self, target): return yaml.load(open(target))
    
    #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    def write_text(self, target, payload):
        with open(target, 'w+') as f:
            f.write(payload)

        return payload

    def write_json(self, target, payload):
        resp = json.dumps(payload)

        return self.write_text(target, resp)

    def write_yaml(self, target, payload):
        resp = yaml.dumps(payload)

        return self.write_text(target, resp)

##################################################################################################################

class JinjaMixin:
    def compile(self, template_source, **context):
        tpl = self._env.from_string(template_source)

        if tpl is not None:
            return tpl.render(context)

        return None

    #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    def renders(self, template_path, **context):
        tpl = self._env.get_template(template_path)

        if tpl is not None:
            return tpl.render(context)

        return None

    #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    def outputs(self, target_path, template_sources, **context):
        if type(template_sources) not in (tuple,set,frozenset,list):
            template_sources = [template_sources]

        resp = None

        for tpl_path in template_sources:
            if resp is None:
                resp = self.renders(tpl_path, **context)

        if resp is not None:
            self.write_text(self.rpath(target_path), resp)

#*****************************************************************************************************************

class Builder(Factory,PosixMixin,JinjaMixin):
    def bpath(self, *parts): return os.path.join(self._pth, *parts)

    def mpath(self, *parts): return self.bpath('mapping', *parts)
    def rpath(self, *parts): return self.bpath('result', self.alias, *parts)

    def spath(self, *parts): return self.rpath('kimino', *parts)
    
    #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    alias = property(lambda self: self._key)

    def initialize(self, alias, bpath=None):
        self._key = alias

        if bpath is not None:
            if not os.path.exists(bpath):
                bpath = None

        if bpath is None:
            bpath = os.path.dirname(os.path.dirname(__file__))

        self._pth = bpath

        self._env = jinja2.Environment(
            loader=jinja2.FileSystemLoader([
                self.mpath('views'),
                self.mpath('theme'),
            ]),
            autoescape=jinja2.select_autoescape(['html', 'xml']),
        auto_reload=True)

        self._dir = []
        self._cdn = {
            '.':   'assets/*',
            'img': ['media/*','import/*'],
            'cdn': 'vendor/*',
        }
        self._tpl = [
            (self.landing_page, None, {}),
        ]
        self._mod = {}

    landing_page = property(lambda self: 'landing.html')
    
    #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    def realm(self, alias, **options):
        if alias not in self._mod:
            if os.path.exists(self.mpath('realms',alias)):
                self._mod[alias] = {
                    'alias': alias,
                }

                for key in ('theme','infos'):
                    pth = self.mpath('realms',alias,'%s.yaml' % key)

                    self._mod[alias][key] = self.read_yaml(pth)

                #self._mod[alias]['alias'] = alias

    def extend(self, alias, parts, context={}):
        self._dir.append(alias)

        for item in parts:
            entry = ('%s/%s.html' % (alias,item), None, context)

            self._tpl.append(entry)

    #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    def prepare(self):
        click.echo('Ensuring Kimino files :')

        if os.path.exists(self.spath('.')):
            click.echo("\t#) Reseting the existing folder ...")

            self.shell('rm','-fR',self.spath('.'))

        #--------------------------------------------------------------------------------------------------

        targets = [
            [self.rpath('.')],
            self._dir,
            [self.spath(x) for x in self._cdn],
            [self.rpath(x) for x in self._mod],
        ]

        for coll in targets:
            for item in coll:
                click.echo("\t*) Ensuring folder : %s" % item)

                self.shell('mkdir','-p',item)

        #--------------------------------------------------------------------------------------------------

        for src,dest in self._cdn.iteritems():
            click.echo("\t-> Copying '%s' to '%s' ..." % (src,dest))

            if type(dest) not in (frozenset,set,tuple,list):
                dest = [dest]

            self.copy(self.spath(src), *[self.mpath(x) for x in dest])

    #+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    def release(self):
        click.echo('Compiling pre-defined views :')

        targets = [(k,v,c) for k,v,c in self._tpl]

        for alias,realm in self._mod.iteritems():
            #for entry in realm.get('theme', {}).get('pages', []):
            for entry in realm['theme']['pages']:
                key = '%s/%s.html' % (alias,entry['view'])
                pth = entry.get('path', None)
                cnt = entry.get('data', {})

                targets.append(tuple([key,pth,cnt]))

        for src,dest,cnt in targets:
            if dest is None:
                dest = src

            if type(dest) is not dict:
                dest = { dest: {} }

            click.echo("\t*) Rendering : %s" % src)

            for pth,cfg in dest.iteritems():
                res = {}

                for x in (cnt,cfg):
                    res.update(x)

                click.echo("\t\t-> Output to : %s" % pth)

                self.outputs(pth, src, **res)

