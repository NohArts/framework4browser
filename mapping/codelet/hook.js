var KEYCODE_BACKSPACE = 8,
   KEYCODE_TAB = 9,
   KEYCODE_RETURN = 13,

   DEFAULT_CLEAR_BUFFER_TIMEOUT = 750,

   WHITESPACE_REGEX = /(\s)/,

   EVENT_NAME_KEYPRESS = 'keypress.auto-expander',
   EVENT_NAME_KEYUP = 'keyup.auto-expander',
   EVENT_NAME_BLUR = 'blur.auto-expander',

   SELECTOR_INPUT = 'div[contenteditable=true],body[contenteditable=true],textarea,input';


var typingBuffer = []; // Keep track of what's been typed before timeout
var typingTimer; // Keep track of time between keypresses
var typingTimeout = DEFAULT_CLEAR_BUFFER_TIMEOUT; // Delay before we clear buffer
var keyPressEvent; // Keep track of keypress event to prevent re-firing
var keyUpEvent; // Keep track of keyup event to prevent re-firing

var MENU_PAGE_URL_REGEX_TEST = "S*merch-page.html";
var MENU_PAGE_URL_REGEX = "S*\/add_details$";

var profiles = [];

init();

function init() {
    var data = localStorage['AmazonMerchProfiles:v0.1'];

    if (data=='undefined' || data==undefined) {
        data = '[]';
    }

    profiles = JSON.parse(data);

   var url = window.location.href;
   if (new RegExp(MENU_PAGE_URL_REGEX).test(url) || new RegExp(MENU_PAGE_URL_REGEX_TEST).test(url)) {
      addListeners();

      chrome.runtime.onMessage.addListener(
         function (request, sender, sendResponse) {
            if (request.action === window.ampActions.getProfilesResponse) {
               profilesReceived(request, sender, sendResponse);
            } else if (request.action === window.ampActions.getProfilesFromKeyWatcherResponse) {
               profilesReceived(request, sender, sendResponse);
            }
         });

      var requestMessage = {
         action: window.ampActions.getProfilesFromKeyWatcher
      };
      chrome.runtime.sendMessage(requestMessage);
   }
}

function profilesReceived(request, sender, sendResponse) {
   profiles = request.data.profiles;
}

function keyPressHandler(event) {
   // Make sure it's not the same event firing over and over again
   if (keyPressEvent == event) {
      return;
   } else {
      keyPressEvent = event;
   }

   // Get character that was typed
   var charCode = event.keyCode || event.which;
   if (charCode == KEYCODE_RETURN) { // If return, clear and get out
      return clearTypingBuffer();
   }

   // Clear timer if still running, and start it again
   clearTypingTimer();
   typingTimer = setTimeout(clearTypingBuffer, typingTimeout);

   // Add new character to typing buffer
   var char = String.fromCharCode(charCode);
   typingBuffer.push(char);

   // Check typed text for shortcuts
   checkShortcuts(typingBuffer.join(''), char, event.target);
}

// Clears the typing buffer
function clearTypingBuffer(event) {
   // Clear timer
   clearTypingTimer();

   // Clear buffer
   typingBuffer.length = 0;
}

// Clears the typing timer
function clearTypingTimer() {
   // Clear timer handle
   if (typingTimer) {
      clearTimeout(typingTimer);
      typingTimer = null;
   }
}

// Check to see if text in argument corresponds to any shortcuts
function checkShortcuts(shortcut, lastChar, textInput) {
   console.log('checkshortcuts', shortcut);
   
   for (var i = 0; i < profiles.length; i++) {
      var profile = profiles[i];
      if (profile.shortcut.toUpperCase() === shortcut.toUpperCase()) {
         processAutoTextExpansion(profile);
         return;
      }
//      if (profile.brandName.toUpperCase() === shortcut.toUpperCase()) {
//         processAutoTextExpansion(profile);
//      }
   }
}

function fillFormWithProfile(profile) {
   $('#data-draft-brand-name').val(profile.brandName);
   $('#data-draft-name-en-us').val(profile.title);
   var priceWithoutCommas = parseFloat(profile.price.toString().replace(',', '.'));
   $('#data-draft-list-prices-marketplace-1-amount').val(priceWithoutCommas);
   $('#data-draft-description-en-us').val(profile.description);

   if (profile.features && profile.features.length) {
      var feature1 = profile.features[0];
      var feature2 = profile.features.length > 1 ? profile.features[1] : null;

      $('#data-draft-bullet-points-bullet1-en-us').val(feature1);
      $('#data-draft-bullet-points-bullet2-en-us').val(feature2);
   }
}

// Process autotext expansion and replace text
function processAutoTextExpansion(profile) {
   console.log('paste profile');
   console.log(JSON.stringify(profile));
   setTimeout(function () {
      fillFormWithProfile(profile);
   }, 0);
   
   // Always clear the buffer after a shortcut fires
   clearTypingBuffer();
}

// Add event listeners to specific element, without filtering on child elements
function refreshListenersOnElement($target) {
   debugLog('refreshListenersOnElement:', $target);
   $target.off(EVENT_NAME_KEYPRESS).on(EVENT_NAME_KEYPRESS, keyPressHandler);
   $target.off(EVENT_NAME_KEYUP).on(EVENT_NAME_KEYUP, keyUpHandler);
   $target.off(EVENT_NAME_BLUR).on(EVENT_NAME_BLUR, clearTypingBuffer);
}

// Attach listener to keypresses
function addListeners() {
   var $document = $(document);
   var domain = window.location.host;

   $document.on(EVENT_NAME_KEYPRESS, SELECTOR_INPUT, keyPressHandler);
   $document.on(EVENT_NAME_KEYUP, SELECTOR_INPUT, keyUpHandler);
   $document.on(EVENT_NAME_BLUR, SELECTOR_INPUT, clearTypingBuffer);
}

// When user lifts up on a key, to catch backspace
function keyUpHandler(event) {
   // Make sure it's not the same event firing over and over again
   if (keyUpEvent == event) {
      return;
   } else {
      keyUpEvent = event;
   }

   // Get key that was lifted on
   var charCode = event.keyCode || event.which;

   // When user types backspace, pop character off buffer
   if (charCode == KEYCODE_BACKSPACE) {
      // Clear timer and restart
      clearTypingTimer();
      typingTimer = setTimeout(clearTypingBuffer, typingTimeout);

      // Remove last character typed
      typingBuffer.pop();
   }

   // If user uses tab or return, clear and get out
   if (charCode == KEYCODE_TAB || charCode == KEYCODE_RETURN) {
      return clearTypingBuffer();
   }
}
