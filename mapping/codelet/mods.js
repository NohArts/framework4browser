$(document).ready(function(){
    /* --------------------------------------------------------
	Components
    -----------------------------------------------------------*/
    (function(){
        /* Textarea */
	if($('.auto-size')[0]) {
	    $('.auto-size').autosize();
	}

        //Select
	if($('.select')[0]) {
	    $('.select').selectpicker();
	}
        
        //Sortable
        if($('.sortable')[0]) {
	    $('.sortable').sortable();
	}
	
        //Tag Select
	if($('.tag-select')[0]) {
	    $('.tag-select').chosen();
	}
        
        /* Tab */
	if($('.tab')[0]) {
	    $('.tab a').click(function(e) {
		e.preventDefault();
		$(this).tab('show');
	    });
	}
        
        /* Collapse */
	if($('.collapse')[0]) {
	    $('.collapse').collapse();
	}
        
        /* Accordion */
        $('.panel-collapse').on('shown.bs.collapse', function () {
            $(this).prev().find('.panel-title a').removeClass('active');
        });

        $('.panel-collapse').on('hidden.bs.collapse', function () {
            $(this).prev().find('.panel-title a').addClass('active');
        });

        //Popover
    	if($('.pover')[0]) {
    	    $('.pover').popover();
    	} 
    })();

    /* ---------------------------
     Vertical tab
     --------------------------- */
    (function(){
        $('.tab-vertical').each(function(){
            var tabHeight = $(this).outerHeight();
            var tabContentHeight = $(this).closest('.tab-container').find('.tab-content').outerHeight();

            if ((tabContentHeight) > (tabHeight)) {
                $(this).height(tabContentHeight);
            }
        })

        $('body').on('click touchstart', '.tab-vertical li', function(){
            var tabVertical = $(this).closest('.tab-vertical');
            tabVertical.height('auto');

            var tabHeight = tabVertical.outerHeight();
            var tabContentHeight = $(this).closest('.tab-container').find('.tab-content').outerHeight();

            if ((tabContentHeight) > (tabHeight)) {
                tabVertical.height(tabContentHeight);
            }
        });
    })();

    /* --------------------------------------------------------
	Custom Scrollbar
    -----------------------------------------------------------*/
    (function() {
	    if($('.overflow')[0]) {
            /*
	        var overflowRegular, overflowInvisible = false;
	        overflowRegular = $('.overflow').niceScroll();
            //*/
	    }
    })();
});
/*
$('table').dataTable({
  "aaData": yourCollection.toJSON(),

  "aoColumns": [
    { "sTitle": "Engine",   "mDataProp": "engine" },
    { "sTitle": "Browser",  "mDataProp": "browser" },
    { "sTitle": "Platform", "mDataProp": "platform" },
    { "sTitle": "Version",  "mDataProp": "version" },
    { "sTitle": "Grade",    "mDataProp": "grade" }
  ]
});
//*/
$(document).ready(function(){
    /* --------------------------------------------------------
	Form Validation
    -----------------------------------------------------------*/
    (function(){
	if($("[class*='form-validation']")[0]) {
	    $("[class*='form-validation']").validationEngine();

	    //Clear Prompt
	    $('body').on('click', '.validation-clear', function(e){
		e.preventDefault();
		$(this).closest('form').validationEngine('hide');
	    });
	}
    })();

    /* --------------------------------------------------------
     `Color Picker
    -----------------------------------------------------------*/
    (function(){
        //Default - hex
	if($('.color-picker')[0]) {
	    $('.color-picker').colorpicker();
	}
        
        //RGB
	if($('.color-picker-rgb')[0]) {
	    $('.color-picker-rgb').colorpicker({
		format: 'rgb'
	    });
	}
        
        //RGBA
	if($('.color-picker-rgba')[0]) {
	    $('.color-picker-rgba').colorpicker({
		format: 'rgba'
	    });
	}
	
	//Output Color
	if($('[class*="color-picker"]')[0]) {
	    $('[class*="color-picker"]').colorpicker().on('changeColor', function(e){
		var colorThis = $(this).val();
		$(this).closest('.color-pick').find('.color-preview').css('background',e.color.toHex());
	    });
	}
    })();

    /* --------------------------------------------------------
     Date Time Picker
     -----------------------------------------------------------*/
    (function(){
        //Date Only
	if($('.date-only')[0]) {
	    $('.date-only').datetimepicker({
		pickTime: false
	    });
	}

        //Time only
	if($('.time-only')[0]) {
	    $('.time-only').datetimepicker({
		pickDate: false
	    });
	}

        //12 Hour Time
	if($('.time-only-12')[0]) {
	    $('.time-only-12').datetimepicker({
		pickDate: false,
		pick12HourFormat: true
	    });
	}
        
        $('.datetime-pick input:text').on('click', function(){
            $(this).closest('.datetime-pick').find('.add-on i').click();
        });
    })();

    
    /* --------------------------------------------------------
     Checkbox + Radio
     -----------------------------------------------------------*/
    if($('input:checkbox, input:radio')[0]) {
	    //Checkbox + Radio skin
	    $('input:checkbox:not([data-toggle="buttons"] input, .make-switch input), input:radio:not([data-toggle="buttons"] input)').iCheck({
		        checkboxClass: 'icheckbox_minimal',
		        radioClass: 'iradio_minimal',
		        increaseArea: '20%' // optional
	    });
        
	    //Checkbox listing
	    var parentCheck = $('.list-parent-check');
	    var listCheck = $('.list-check');
        
	    parentCheck.on('ifChecked', function(){
		    $(this).closest('.list-container').find('.list-check').iCheck('check');
	    });
        
	    parentCheck.on('ifClicked', function(){
		    $(this).closest('.list-container').find('.list-check').iCheck('uncheck');
	    });
        
	    listCheck.on('ifChecked', function(){
		        var parent = $(this).closest('.list-container').find('.list-parent-check');
		        var thisCheck = $(this).closest('.list-container').find('.list-check');
		        var thisChecked = $(this).closest('.list-container').find('.list-check:checked');
	        
		        if(thisCheck.length == thisChecked.length) {
			    parent.iCheck('check');
		        }
	    });
        
	    listCheck.on('ifUnchecked', function(){
		        var parent = $(this).closest('.list-container').find('.list-parent-check');
		        parent.iCheck('uncheck');
	    });
        
	    listCheck.on('ifChanged', function(){
		        var thisChecked = $(this).closest('.list-container').find('.list-check:checked');
		        var showon = $(this).closest('.list-container').find('.show-on');
		        if(thisChecked.length > 0 ) {
			    showon.show();
		        }
		        else {
			    showon.hide();
		        }
	    });
    }
});
$(document).ready(function(){
    /* --------------------------------------------------------
     WYSIWYE Editor + Markedown
     -----------------------------------------------------------*/
    (function(){
        //Markedown
	    if($('.markdown-editor')[0]) {
	        $('.markdown-editor').markdown({
		    autofocus:false,
		    savable:false
	        });
	    }
            
            //WYSIWYE Editor
	    if($('.wysiwye-editor')[0]) {
	        $('.wysiwye-editor').summernote({
		    height: 200
	        });
	    }
    })();

    /* --------------------------------------------------------
     Media Player
     -----------------------------------------------------------*/
    (function(){
	    if($('audio, video')[0]) {
	        $('audio,video').mediaelementplayer({
		    success: function(player, node) {
		        $('#' + node.id + '-mode').html('mode: ' + player.pluginType);
		    }
	        });
	    }
    })();

    /* ---------------------------
	Image Popup [Pirobox]
    --------------------------- */
    (function() {
	    if($('.pirobox_gall')[0]) {
	        //Fix IE
	        jQuery.browser = {};
	        (function () {
		    jQuery.browser.msie = false;
		    jQuery.browser.version = 0;
		    if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
		        jQuery.browser.msie = true;
		        jQuery.browser.version = RegExp.$1;
		    }
	        })();
	        
	        //Lightbox
	        $().piroBox_ext({
		    piro_speed : 700,
		    bg_alpha : 0.5,
		    piro_scroll : true // pirobox always positioned at the center of the page
	        });
	    }
    })();

    /* --------------------------------------------------------
	Photo Gallery
    -----------------------------------------------------------*/
    (function(){
        if($('.photo-gallery')[0]){
            $('.photo-gallery').SuperBox();
        }
    })();
});

