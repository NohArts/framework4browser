const path = require('path')
const url = require('url')

const electron = require('electron')
const protocols = require('electron-protocols');

const app = electron.app;
const BrowserWindow = electron.BrowserWindow;

/*
const kernel = require('./kernel/__index.js');

let config = kernel.commons.electron({
    theme: process.env.THEME_ID || 'color-admin',
    proto: [],
})
//*/

let config = {
    rpath: path.dirname(__dirname),
    proto: [],
}

let mainWindow;

for (var i=0 ; i<config.proto.length ; i++) {
    let base = config.proto[i].path

    protocols.register(config.proto[i].name, );
}

app.commandLine.appendSwitch('disable-web-security');

function createWindow () {
    mainWindow = new BrowserWindow({
        'width': 1024, 'height': 740,
        'web-preferences': {
            'web-security': false
        }
    })

    mainWindow.loadURL(url.format({
        pathname: config.rpath,
        protocol: 'file:',
        slashes: true
    }))

    //mainWindow.webContents.openDevTools();

    mainWindow.on('closed', function () {
        mainWindow = null;
    })
}

app.on('ready', createWindow);

app.on('window-all-closed', function () {
    if (process.platform !== 'darwin') {
        app.quit();
    }
})

app.on('activate', function () {
    if (mainWindow === null) {
        createWindow()
    }
})

//      ../../skins/acme-dash

