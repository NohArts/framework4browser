init();

function init() {
   chrome.runtime.onMessage.addListener(
      function (request, sender, sendResponse) {
         if (request.action === window.ampActions.prefillWithProfile) {
               console.log('profileReceived');
               var profile = request.data.profileInfo;
               console.log(profile);

               $('#data-draft-brand-name').val(profile.brandName);
               $('#data-draft-name-en-us').val(profile.title);
               $('#data-draft-list-prices-marketplace-1-amount').val(profile.price);
               $('#data-draft-description-en-us').val(profile.description);

               if (profile.features && profile.features.length) {
                  var feature1 = profile.features[0];
                  var feature2 = profile.features.length > 1 ? profile.features[1] : null;

                  $('#data-draft-bullet-points-bullet1-en-us').val(feature1);
                  $('#data-draft-bullet-points-bullet2-en-us').val(feature2);
               }
         }
      });
}

